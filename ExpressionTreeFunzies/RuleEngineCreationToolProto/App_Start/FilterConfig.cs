﻿using System.Web;
using System.Web.Mvc;

namespace RuleEngineCreationToolProto
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
