﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using RulesEngine.Providers;

namespace RulesEngine
{
    public class Rule
    {
        private Delegate _compiledDelegate;
        public Dictionary<Type, ParameterExpression> ProviderList { get; set; }
        public Expression ExpressionTree { get; set; }

        public Delegate CompiledDelegate
        {
            get { return _compiledDelegate ?? (_compiledDelegate = this.Compile()); }
            set { _compiledDelegate = value; }
        }
        private Delegate Compile()
        {
            var delegateParamTypes = new List<Type>(ProviderList.Keys) { typeof(bool) }.ToArray();
            var functype = Expression.GetFuncType(delegateParamTypes);
            return Expression.Lambda(functype, ExpressionTree, ProviderList.Values.ToArray()).Compile();
        }
        public Rule()
        {
            ProviderList = new Dictionary<Type, ParameterExpression>();
        }
        public Rule(Expression expression, Dictionary<Type, ParameterExpression> providerList)
        {
            ExpressionTree = expression;
            ProviderList = providerList;
        }
    }
}
