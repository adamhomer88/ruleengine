﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using RulesEngine.Providers;

namespace RulesEngine
{
    public class RuleParser
    {
        private readonly List<string> _compositOperators = new List<string>(){"And", "Or"};  
        public enum ParserState
        {
            Left, Right, Operator
        }
        private Queue<string> _outputQueue = new Queue<string>();
        private Stack<string> _functionStack = new Stack<string>();
        public ParserState State { get; set; }
        public Rule Parse(Queue<string> ruleQueue)
        {
            Rule rule = new Rule();
            Stack<Expression> tokenStack = new Stack<Expression>();
            while (ruleQueue.Count > 0)
            {
                string token;
                if (_compositOperators.Contains(token = ruleQueue.Dequeue()))
                {
                    // Check if binary or unary operator (currently the operator is always binary)
                    Expression left = tokenStack.Pop();
                    Expression right = tokenStack.Pop();
                    ExpressionType tbinary;
                    if (Enum.TryParse(token, out tbinary))
                    {
                        var binaryExpression = Expression.MakeBinary(tbinary, left, right);
                        tokenStack.Push(binaryExpression);
                    }
                }
                else
                {
                    string[] expressionTokens = token.Split(' ');
                    // What are the possible token patterns here? 
                    // Provider Attribute Method Method....etc
                    // Constant Method Method...etc
                    // Any potential attribute could also be a method and vice versa
                    var leftExpression = ParseOperand(expressionTokens[0], rule.ProviderList);
                    var rightExpression = ParseOperand(expressionTokens[2], rule.ProviderList);
                    ExpressionType compositeExpression;
                    Enum.TryParse(expressionTokens[1], out compositeExpression);

                    //TODO This will need to be expanded to handle unary expressions.
                    tokenStack.Push(Expression.MakeBinary(compositeExpression, leftExpression, rightExpression));
                }
            }
            rule.ExpressionTree = tokenStack.Pop();
            return rule;
        }

        private Expression ParseOperand(string operandStr, Dictionary<Type, ParameterExpression> providerList)
        {
            Expression operand = null;
            // is constant.
            var operandParts = operandStr.Split('.'); 
            Expression firstPart = null;
            foreach (var part in operandParts)
            {
                if (operand == null)
                {
                    Func<AssemblyName, Assembly> assemblyResolver = name => Assembly.GetAssembly(typeof(Provider));
                    var providerType = Type.GetType("RulesEngine.Providers." + part);
                    // This is making an assumption that all objects are going to be of a provider type.
                    // In the case of non-traditional dataproviders we would need to create a provider for that object. e.g. Applicants.
                    if (providerType != null && providerType.IsSubclassOf(typeof (Provider)))
                    {
                        ParameterExpression providerParam = null;
                        if (!providerList.TryGetValue(providerType, out providerParam))
                        {
                            providerParam = Expression.Parameter(providerType, part);
                            providerList.Add(providerType, providerParam);
                        }
                        firstPart = providerParam;
                        operand = firstPart;
                    }
                    else
                    {
                        //This is a constant. Right?
                        var scrubbedPart = part.Replace(',', '.');
                        decimal d;
                        firstPart = decimal.TryParse(scrubbedPart, out d) ? Expression.Constant(d, d.GetType()) : Expression.Constant(scrubbedPart, scrubbedPart.GetType());
                        operand = firstPart;
                    }
                }
                else
                {
                    // This will either be a property or method call.
                    // Does it matter if it's a constant or provider? Either type can have methods OR properties.
                    MethodInfo method;
                    Type test = operand.Type;
                    if ((method = operand.Type.GetMethod(part,Type.EmptyTypes))!=null)
                    {
                        // This part is a method call. Currently only support 0 argument methods
                        operand = Expression.Call(operand, method);
                    }
                    else
                    {
                        // this part MUST be a property or field.
                        operand = Expression.PropertyOrField(operand, part);
                    }
                }
            }

            return operand;
        }

        public Queue<string> ShuntingYard(List<string> tokens)
        {
            Queue<string> OutputQueue = new Queue<string>();
            Stack<string> OperatorStack = new Stack<string>();

            foreach (var token in tokens)
            {
                if (_compositOperators.Contains(token))
                {
                    OperatorStack.Push(token);
                }
                else if (token.Equals("("))
                {
                    OperatorStack.Push(token);
                }
                else if (token.Equals(")"))
                {
                    string poppedToken;
                    while (!(poppedToken = OperatorStack.Pop()).Equals("("))
                    {
                        OutputQueue.Enqueue(poppedToken);
                    }
                }
                else
                {
                    OutputQueue.Enqueue(token);
                }
            }
            while(OperatorStack.Count!=0)
            {
                var token = OperatorStack.Pop();
                if (_compositOperators.Contains(token))
                {
                    OutputQueue.Enqueue(token);
                }
            }

            return OutputQueue;
        }
    }
}
