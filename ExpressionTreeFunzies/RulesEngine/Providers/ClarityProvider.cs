﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RulesEngine.Providers
{
    public class ClarityProvider : Provider
    {
        public string BankCode { get; set; }
        public decimal Threshold { get; set; }
        public decimal Something { get; set; }

        public ClarityProvider()
        {
            Id = 2;
            Name = "Clarity";
            BankCode = "LMNOP";
            Threshold = 1.5m;
            Something = 2.5m;
        }
    }
}
