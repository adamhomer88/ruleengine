﻿using System.Runtime.Serialization;

namespace RulesEngine.Providers
{
    [DataContract]
    public class DataXProvider : Provider
    {
        public DataXProvider()
        {
            Id = 1;
            Name = "DataX";
            Threshold = 2.2m;
            Something = 4;
            BankCode = "P2";
        }
        [DataMember]
        public string BankCode { get; set; }
        public decimal Threshold { get; set; }
        public decimal Something { get; set; }
    }
}
