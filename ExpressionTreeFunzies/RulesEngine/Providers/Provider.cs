﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RulesEngine.Providers
{
    [DataContract]
    [KnownType("GetKnownTypes")]
    public class Provider
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<string> ErrorCodes { get; set; }
        public static List<Type> KnownTypes { get; private set; }
        public static List<Type> GetKnownTypes()
        {
            return KnownTypes;
        }
        static Provider()
        {
            KnownTypes = new List<Type>();
            try
            {
                foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
                {
                    if (!type.IsAbstract && type.IsSubclassOf(typeof(Provider)))
                    {
                        KnownTypes.Add(type);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error!");
            }
        }
    }
}
