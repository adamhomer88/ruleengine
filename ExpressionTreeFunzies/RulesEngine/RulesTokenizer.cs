﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RulesEngine
{
    public class RulesTokenizer
    {
        public List<string> Tokenize(string expression)
        {
            var tokens = new List<string>();
            var characters = expression.ToArray();
            for (var i = 0; i < characters.Length; i++)
            {
                switch (characters[i])
                {
                    case '(': tokens.Add(characters[i].ToString());
                        break;
                    case ')': tokens.Add(characters[i].ToString());
                        break;
                    default:

                        StringBuilder tokenBuilder = new StringBuilder();
                        do
                        {
                            tokenBuilder.Append(characters[i]);
                            i++;
                        } while (characters[i] != '(' && characters[i] != ')');
                        tokens.Add(tokenBuilder.ToString().Trim());
                        i--;
                        break;
                }
            }
            return tokens;
        }
    }

}
