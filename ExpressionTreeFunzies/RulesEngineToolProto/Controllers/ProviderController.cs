﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RulesEngine.Providers;

namespace RulesEngineToolProto.Controllers
{
    public class ProviderController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new List<Provider>{ new DataXProvider()});
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}