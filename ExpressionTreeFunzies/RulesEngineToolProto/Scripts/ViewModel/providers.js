﻿function ViewModel() {
    var self = this;
    self.providers = ko.observableArray([]);
    self.selectedProvider = ko.observable();
    self.Load = function () {
        $.get("/api/provider", function (data) {
            data.forEach(function (provider) {
                self.providers.push(provider);
            });
        });
        return self;
    }
}

var Country = function (name, population) {
    this.countryName = name;
    this.countryPopulation = population;
};

var viewModel = {
    availableCountries: ko.observableArray([
        new Country("UK", 65000000),
        new Country("USA", 320000000),
        new Country("Sweden", 29000000)
    ]),
    selectedCountry: ko.observable() // Nothing selected by default
};

var view = new ViewModel();
ko.applyBindings(view.Load());