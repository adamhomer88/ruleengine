﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using RulesEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RulesEngine.Providers;

namespace RulesEngineTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RuleParserTest()
        {
            RuleParser parser = new RuleParser();

            // (Threshold > 2) && (BankCode != XD00 OR Something < 0)
            var reversePolishQueue = parser.ShuntingYard(
                new RulesTokenizer().Tokenize(
                    "(Threshold GreaterThan 2) AND ((BankCode NotEquals XD00) OR (Something LessThan 0))"));

            Console.Out.WriteLine(reversePolishQueue.ToString());

            // (Threshold > 2 && BankCode != XD00) OR (Something < 0)
            reversePolishQueue =
                parser.ShuntingYard(
                    new RulesTokenizer().Tokenize(
                        "((Threshold GreaterThan 2) AND (BankCode NotEquals XD00)) OR (Something LessThan 0)"));
            Console.Out.WriteLine(reversePolishQueue.ToString());

            // (Threshold == 2 AND BankCode == P2) OR (Threshold ==1 AND BankCode == P1)
            reversePolishQueue =
                parser.ShuntingYard(
                    new RulesTokenizer().Tokenize(
                        "((DataX.Threshold Equals 2) AND (BankCode Equals P2)) OR ((Threshold Equals 1) AND (BankCode Equals P1))"));
            Console.Out.WriteLine(reversePolishQueue.ToString());
        }

        [TestMethod]
        public void TestComplexRuleParser()
        {
            // This test needs to be re-written
            //RuleParser parser = new RuleParser();
            //var reversePolishQueue =
            //    parser.ShuntingYard(
            //        new RulesTokenizer().Tokenize(
            //            "((Threshold Equal 2) And (BankCode Equal P2)) Or ((Threshold Equal 1) And (BankCode Equal P1))"));
            //var thisisit = parser.Parse(reversePolishQueue);
            //var thisisreallyit = Expression.Lambda<Func<DataXProvider,bool>>(thisisit, Rule.ProviderList[typeof(DataXProvider)]);
            //var thisisbroken = thisisreallyit.Compile();
            //var provider = new DataXProvider();
            //Assert.IsFalse(thisisbroken(new DataXProvider()));
            //provider.BankCode = "P2";
            //Assert.IsTrue(thisisbroken(provider));
            //provider.Threshold = 1;
            //provider.BankCode = "P1";
            //Assert.IsTrue(thisisbroken(provider));
        }

        [TestMethod]
        public void TokenizerTest()
        {
            RulesTokenizer tokenizer = new RulesTokenizer();
            var tokens =
                tokenizer.Tokenize("(Threshold GreaterThan 2) AND ((BankCode NotEquals XD00) OR (Something LessThan 0))");
        }

        [TestMethod]
        public void ParameterExpressionEqualstest()
        {
            var exp1 = Expression.Parameter(typeof(DataXProvider));
            var exp2 = Expression.Parameter(typeof(DataXProvider));
            
            Assert.AreEqual(exp1.Type, exp2.Type);
        }

        [TestMethod]
        public void TestMethodChainCall()
        {
            // Test setup
            var expression =
                "((DataXProvider.Threshold Equal 2,2) And (DataXProvider.BankCode.ToLower Equal p2)) Or ((DataXProvider.Threshold Equal 1) And (ClarityProvider.BankCode Equal P1))";
            RulesTokenizer tokenizer = new RulesTokenizer();
            var tokens = tokenizer.Tokenize(expression);
            RuleParser parser = new RuleParser();
            var rpnQueue = parser.ShuntingYard(tokens);
            var rule = parser.Parse(rpnQueue);
            var compiledDelegate = rule.CompiledDelegate;

            // Tests
            var datax = new DataXProvider();
            var clarity = new ClarityProvider();
            object[] providers = {datax, clarity};
            Assert.IsTrue((bool)compiledDelegate.DynamicInvoke(providers));

            datax.Threshold = 1m;
            clarity.BankCode = "P1";
            Assert.IsTrue((bool)compiledDelegate.DynamicInvoke(providers));

            datax.Threshold = 2.2m;
            datax.BankCode = "XD00";
            clarity.BankCode = "LLOLL";
            Assert.IsFalse((bool)compiledDelegate.DynamicInvoke(providers));
        }
        //[TestMethod]
        //public void TestMethod2()
        //{
        //    var method = typeof (Expression).GetMethod("GetFuncType");
        //    var genericmethod = method.MakeGenericMethod(typeof (DataXProvider), typeof (Provider), typeof(bool));
        //    genericmethod.Invoke()
        //    var parameters = method.GetParameters();

        //}
    }
}
